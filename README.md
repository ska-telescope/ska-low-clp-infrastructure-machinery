# Digital Signal PSI infrastructure

[![Documentation Status](https://readthedocs.org/projects/ska-telescope-ska-low-clp-infrastructure-machinery/badge/?version=latest)](https://developer.skao.int/projects/ska-low-clp-infrastructure-machinery/en/latest/)

The Infra Machinery is used to manage the infrastructure supervised by SKAO.
It uses [Ansible](https://www.ansible.com/) for installation/configuration.

## Prerequisites

- [Poetry](https://python-poetry.org)
- [Vault CLI](https://developer.hashicorp.com/vault/install)

Also check the READMEs of each submodule for an updated list of requirements:

- [SKA Ansible Collections](https://gitlab.com/ska-telescope/sdi/ska-ser-ansible-collections/-/blob/main/README.md#requirements)
- [SKA Makefile](https://gitlab.com/ska-telescope/sdi/ska-cicd-makefile/-/blob/main/README.md)

## Setup

After cloning this repository, make sure to initialize the submodules:

```sh
git submodule update --init
```

Then, install the Poetry dependencies:

```sh
poetry install
```

Finally, install the required Ansible collections and roles:

```sh
poetry run make install
```

## How to use

Note: this README assumes that all `make` invocations are performed in the Poetry virtual environment.
Either use `poetry shell` to spawn a shell in the virtual environment, or prepend `poetry run` to each command.

### Make Targets

The `Makefile` has a help target to show all available targets:

```sh
make help
```

The target called `playbooks` which redirects commands and variables to the [`ska-ser-ansible-collections` Makefile](./ska-ser-ansible-collections/Makefile).

Always check the READMEs for [`ska-ser-ansible-collections`](https://gitlab.com/ska-telescope/sdi/ska-ser-ansible-collections/-/blob/main/README.md)
for up-to-date setup and how to use recommendations.
