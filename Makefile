SHELL = /usr/bin/env bash
MAKEFLAGS += --no-print-directory

DATACENTRE ?= digital-signal-psi
ENVIRONMENT ?= production

BASE_PATH ?= $(shell cd "$(dirname "$1")"; pwd -P)

VAULT_TOKEN_PATH ?= $${HOME}
VAULT_URL ?= https://vault.skao.int

ENVIRONMENT_ROOT_DIR ?= $(BASE_PATH)
INVENTORY ?= $(ENVIRONMENT_ROOT_DIR)/hosts
ANSIBLE_CONFIG ?= "$(ENVIRONMENT_ROOT_DIR)/ansible.cfg"
ANSIBLE_SSH_ARGS ?= -o ControlPersist=30m -o ControlMaster=auto -o StrictHostKeyChecking=no
ANSIBLE_COLLECTIONS_PATH ?= $(BASE_PATH)/ska-ser-ansible-collections:~/.ansible/collections

DEFAULT_TEXT_EDITOR ?= vi
DOCS_SPHINXOPTS = -W --keep-going
ANSIBLE_EXTRA_VARS ?= --extra-vars 'ska_datacentre=$(DATACENTRE) ska_environment=$(ENVIRONMENT)'

ENV_VARS ?= \
	DATACENTRE="$(DATACENTRE)" \
	ENVIRONMENT="$(ENVIRONMENT)" \
	BASE_PATH="$(BASE_PATH)" \
	ENVIRONMENT_ROOT_DIR="$(ENVIRONMENT_ROOT_DIR)" \
	INVENTORY="$(INVENTORY)" \
	ANSIBLE_CONFIG="$(ANSIBLE_CONFIG)" \
	ANSIBLE_SSH_ARGS="$(ANSIBLE_SSH_ARGS)" \
	ANSIBLE_COLLECTIONS_PATH="$(ANSIBLE_COLLECTIONS_PATH)" \
	ANSIBLE_EXTRA_VARS="$(ANSIBLE_EXTRA_VARS)" \
	ANSIBLE_HASHI_VAULT_TOKEN_PATH="$(VAULT_TOKEN_PATH)" \
	ANSIBLE_HASHI_VAULT_ADDR="$(VAULT_URL)"

playbook ?=
hosts ?=
group ?= all
opts ?= $(args)
ifneq ("$(hosts)", "")
  opts := $(opts) --extra-vars 'target_hosts="$(hosts)"'
endif
ifneq ("$(limit)", "")
  opts := $(opts) --limit="$(limit)"
endif
ifneq ("$(tag)", "")
  opts := $(opts) --tag="$(tag)"
endif

##@ Running playbooks

.PHONY: run
run: required-var-playbook vault-login ## make run <playbook=playbooks/example.yml> [hosts=<ansible hosts selector>] [tag=<ansible tag>] [limit=<ansible host limit>] [args=<ansible-playbook arguments>]  # Run a local playbook
	@$(ENV_VARS) ansible-playbook -i $(INVENTORY) $(opts) $(ANSIBLE_EXTRA_VARS) "$(playbook)"

# If the first argument is "playbooks"...
ifeq (playbooks,$(firstword $(MAKECMDGOALS)))
  # use the rest as arguments for "playbooks"
  TARGET_ARGS := $(wordlist 2,$(words $(MAKECMDGOALS)),$(MAKECMDGOALS))
  # ...and turn them into do-nothing targets
  $(eval $(TARGET_ARGS):;@:)
endif

.PHONY: playbooks
playbooks: vault-login  ## make playbooks <job> <target>  # Run a playbook defined in ska-ser-ansible-collections
	@cd ska-ser-ansible-collections && $(ENV_VARS) $(MAKE) $(TARGET_ARGS)

##@ Utilities

.PHONY: install
install: ## make install  # Install dependencies from Ansible Galaxy
	@ansible-galaxy install --ignore-errors -r requirements.yml $(opts)

.PHONY: lint
lint: ## make lint  # Lint playbooks and inventories using ansible-lint
	@ANSIBLE_COLLECTIONS_PATH=$(ANSIBLE_COLLECTIONS_PATH) ansible-lint group_vars host_vars playbooks

.PHONY: console
console: ## make console [args=<ansible-console arguments>]  # Run an ansible console
	@$(ENV_VARS) ansible-console -i $(INVENTORY) $(opts)

.PHONY: inventory-graph
inventory-graph: ## make inventory-graph  # Display the inventory as seen from Ansible
	@$(ENV_VARS) ansible-inventory --graph -i $(INVENTORY) $(opts)

.PHONY: inventory-list
inventory-list: ## make inventory-list  # Display the inventory as seen from Ansible
	@$(ENV_VARS) ansible-inventory --list -i $(INVENTORY) $(opts)

.PHONY: list
list: ## make list [group=all]  # List hosts inventory
	@$(ENV_VARS) ansible -i $(INVENTORY) $(group) --list-hosts

##@ Miscellaneous

-include .make/docs.mk
-include .make/make.mk
-include .make/metrics.mk

.PHONY: help
help:  ## Display this help
	@awk 'BEGIN {FS = ":.*##"; printf "\nUsage:\n  make \033[36m<target>\033[0m\n"} /^[a-zA-Z_-]+:.*?##/ { printf "  \033[36m%-15s\033[0m %s\n", $$1, $$2 } /^##@/ { printf "\n\033[1m%s\033[0m\n", substr($$0, 5) } ' $(MAKEFILE_LIST)

vault-login:
	@VAULT_ADDR=$(VAULT_URL) vault token lookup > /dev/null \
	&& VAULT_ADDR=$(VAULT_URL) vault token renew > /dev/null \
	|| VAULT_ADDR=$(VAULT_URL) vault login -method=oidc

required-var-playbook:
	@[ ! -z $(playbook) ] || (echo "Required variable 'playbook' is missing" && exit 1)
	@stat $(playbook) &> /dev/null || (echo "Playbook '$(playbook)' does not exist" && exit 1)

.DEFAULT_GOAL := help
