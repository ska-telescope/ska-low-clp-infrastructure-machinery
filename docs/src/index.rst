********************************************************************************
ska-low-clp-infrastructure-machinery
********************************************************************************

The `ska-low-clp-infrastructure-machinery`_ repository contains Ansible 
inventories, collections and playbooks to automate the installation of the CLP 
data center.

Instructions on how to set up the repository can be found in its `README.md`_.

================================================================================
About this documentation
================================================================================

Information in this documentation assumes that you have set up your local copy 
of the `ska-low-clp-infrastructure-machinery`_ repository on your machine 
according to the instructions found there.

Since the repository uses `Poetry`_ for its dependency management, each command
shown in the documentation is assumed to be run from within a Poetry virtual
environment. This can be done in two ways:

* Prepend each command with ``poetry run``.
* Enter the virtual environment before running any command using 
  ``poetry shell``.

================================================================================
Ansible vs MAAS
================================================================================

The hosts in the CLP Ansible inventory are provisioned outside of this 
repository using `MAAS`_. It is a combination of VM and bare metal hosts, all
running Ubuntu 22.04 LTS unless otherwise specified.

Through MAAS we manage each host's operating system, network configuration and 
storage layout. After a host is (re-)provisioned, the Ansible inventory is 
updated to reflect the state in MAAS.

Apart from the OS, network and storage, everything in CLP is managed by Ansible
through the `ska-low-clp-infrastructure-machinery`_ repository.

Our MAAS server can be found here: `<http://172.16.0.1:5240/MAAS>`_.

.. toctree::
    :maxdepth: 1
    :caption: Services

    services/nexus

.. toctree::
    :maxdepth: 1
    :caption: Guides

    guides/users
    guides/ceph
    guides/k8s-deploy
    guides/k8s-upgrade
    guides/p4-switch
    guides/tor-switch

.. _ska-low-clp-infrastructure-machinery: https://gitlab.com/ska-telescope/ska-low-clp-infrastructure-machinery
.. _README.md: https://gitlab.com/ska-telescope/ska-low-clp-infrastructure-machinery/-/blob/main/README.md
.. _Poetry: https://python-poetry.org/
.. _MAAS: https://maas.io/
