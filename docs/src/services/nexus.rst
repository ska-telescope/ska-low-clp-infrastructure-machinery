********************************************************************************
Nexus
********************************************************************************

================================================================================
What is Nexus
================================================================================

`Nexus`_ is used by SKAO as artefact repository for things like Python packages, OCI images, Helm charts and raw artefacts.
The `Central Artefact Repository`_ (CAR) is the SKAO-managed instance where all artefacts are stored,
but each facility has the option to deploy their own instance of Nexus to act as a cache, for several reasons:

- To reduce the load on the CAR
- To reduce the time it takes to spin up OCI containers by caching them locally in the facility

Additionally, the Nexus cache instances can be configured with internal hosted repositories for artefacts
not used outside their respective facilities, such as:

- OCI images built for internal applications
- Raw artefacts that should not be hosted publicly (like proprietary software).

================================================================================
Digital Signal PSI Nexus cache instance
================================================================================

The Digital Signal PSI Nexus cache instance can be found at http://nexus.ds-psi.internal.skao.int (VPN required).

It is configured with the following repositories:

Default repositories
--------------------

.. list-table::
    :header-rows: 1

    * - Name
      - Format
      - Type
      - Description
    * - ``central-proxy``
      - docker
      - proxy
      - Points to the CAR repository hosting all OCI images published by SKAO
    * - ``docker-proxy``
      - docker
      - proxy
      - Points to a public Docker registry hosting public OCI images
    * - ``gitlab-proxy``
      - docker
      - proxy
      - Points to Gitlab-hosted Docker registries hosting development builds of SKAO OCI images
    * - ``docker-internal``
      - docker
      - hosted
      - Contains OCI images internal to Digital Signal PSI
    * - ``docker-all``
      - docker
      - group
      - Groups all of the above repositories to act as single mirror used to configure Docker runtimes, see explanation below
    * - ``ubuntu-jammy-proxy``
      - apt
      - proxy
      - Currently not used in Digital Signal PSI
    * - ``ubuntu-security-jammy-proxy``
      - apt
      - proxy
      - Currently not used in Digital Signal PSI

.. note::
    
    All OCI runtimes in the Digital Signal PSI (with the exception of the VM running Nexus itself) are configured to use
    these docker repositories as their mirror, causing Docker, Podman and Kubernetes to pull their OCI images through the cache.

.. _nexus-p4-repo:

``p4-switch-internal`` repository
---------------------------------

| Format: raw
| Type: hosted

This repository is used to host all versions of the Intel P4 SDE used by the LOW-CBF P4 switches.
Since the SDE is proprietary software we are not allowed to publish it anywhere public,
which is why it has to be hosted in each facility separately.

A separate ``p4`` user is configured with permissions to manually upload the SDE tarballs to Nexus through the web UI.
Credentials for this user can be found in `the nexus secrets in Vault`_.

.. _Central Artefact Repository: https://developer.skao.int/en/latest/explanation/central-artefact-repository.html
.. _Nexus: https://www.sonatype.com/products/sonatype-nexus-repository
.. _the nexus secrets in Vault: https://vault.skao.int/ui/vault/secrets/digital-signal-psi/kv/shared/nexus/details
