# Configuration file for the Sphinx documentation builder.
#
# For the full list of built-in configuration values, see the documentation:
# https://www.sphinx-doc.org/en/master/usage/configuration.html

# -- Project information -----------------------------------------------------
# https://www.sphinx-doc.org/en/master/usage/configuration.html#project-information

project = 'ska-low-clp-infrastructure-machinery'
copyright = '2023, TOPIC Team'
author = 'TOPIC Team'
release = '0.1.0'

# -- General configuration ---------------------------------------------------
# https://www.sphinx-doc.org/en/master/usage/configuration.html#general-configuration

extensions = [
    'sphinx.ext.intersphinx',
]

source_suffix = ['.rst']
exclude_patterns = []

pygments_style = 'sphinx'

# -- Options for HTML output -------------------------------------------------
# https://www.sphinx-doc.org/en/master/usage/configuration.html#options-for-html-output

html_theme = 'ska_ser_sphinx_theme'

html_context = {
    'theme_logo_only': True,
    'display_gitlab': True,
    'gitlab_user': 'ska-telescope',
    'gitlab_repo': project,
    'gitlab_version': 'main',
    'conf_py_path': '/docs/src/',  # Path in the checkout to the docs root
    'theme_vcs_pageview_mode': 'edit',
}
