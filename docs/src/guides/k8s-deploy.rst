********************************************************************************
Deploy the Kubernetes cluster
********************************************************************************

This page describes the steps required to deploy a Kubernetes cluster to the 
CLP.

.. note::

    Parts of the CLP Kubernetes cluster use Ceph for the provisioning of
    persistent volumes. Make sure to follow :ref:`deploy-ceph` before deploying
    Kubernetes.

.. _create-cluster:

================================================================================
Forming a new cluster
================================================================================

To form the Kubernetes cluster, start by running the following ``make`` command:

.. code-block:: console

    make run playbook=playbooks/k8s-cluster.yml

Install the Kubernetes tools used to deploy workloads on the control plane:

.. code-block:: console

    make playbooks common k8s-tools-install PLAYBOOKS_HOSTS=k8s_controlplane

================================================================================
Installing services
================================================================================

.. note::

    After the cluster is running, all services installed through playbooks
    only target a single control-plane node, as the resources are created on the
    Kubernetes API, not on the underlying nodes.

    The following steps therefore set the ``PLAYBOOKS_HOSTS`` variable to
    ``k8s_controlplane[0]``. Using any other host will probably fail,
    as these hosts are not equipped with the packages required to submit
    resources to the Kubernetes API.

--------------------------------------------------------------------------------
Install required services
--------------------------------------------------------------------------------

Install the services required for the Kubernetes cluster:

.. code-block:: console

    make playbooks k8s install TAGS=ingress,metallb,metrics,multihoming,kyverno PLAYBOOKS_HOSTS=k8s_controlplane[0]

--------------------------------------------------------------------------------
Set up dynamic PersistentVolume provisioning
--------------------------------------------------------------------------------

Dynamic provisioning of PersistentVolumes is done using  `Rook.io`_, a
Kubernetes plugin that interacts with Ceph.

The Ansible collection to install this plugin uses two config files from the
Ceph cluster to prevent you having to provide all of the settings manually.
So, before installing `Rook.io`_, first copy the ``ceph.conf`` and 
``ceph.admin.client.keyring`` files from one of the Ceph cluster nodes to
``resources/ceph/``:

.. code-block:: console

    rsync clp-k8s-worker-1a:/etc/ceph/ceph.conf resources/ceph/ceph.conf
    rsync --rsync-path="sudo rsync" \
    clp-k8s-worker-1a:/var/lib/ceph/<FSID>/config/ceph.client.admin.keyring \
    resources/ceph/ceph.client.admin.keyring

.. note::

    Replace ``<FSID>`` with the value found in ``ceph.conf``

Then, install `Rook.io`_:

.. code-block:: console

    make playbooks k8s install TAGS=rookio PLAYBOOKS_HOSTS=k8s_controlplane[0]

--------------------------------------------------------------------------------
Set up BinderHub and JupyterHub
--------------------------------------------------------------------------------

The following command installs BinderHub and JupyterHub:

.. code-block:: console

    make run playbook=playbooks/deploy-binderhub.yml

--------------------------------------------------------------------------------
Install SKAO services
--------------------------------------------------------------------------------

This command installs the following SKAO services:

* Engineering Data Archive (EDA)
* Taranta
* TANGO Operator

.. code-block:: console

    make playbooks k8s install TAGS=eda,taranta,ska_tango_operator PLAYBOOKS_HOSTS=k8s_controlplane[0]

================================================================================
Adding nodes to an existing cluster
================================================================================

To add one or more nodes to an existing cluster, simply run the following ``make`` command again:

.. code-block:: console

    make run playbook=playbooks/k8s-cluster.yml

.. _Rook.io: https://rook.io/
