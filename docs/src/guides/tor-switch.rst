********************************************************************************
Initial TOR switch configuration
********************************************************************************

Before the TOR switch can be managed by Ansible, some preliminary configuration
needs to be performed by hand.  By default, the network management port is
configured via DHCP.

If the IP address of the switch is already know, an SSH connection can be
opened directly to the switch; the serial console section below can be skipped.

When presented with a login prompt, log in as the ``admin`` user.  The default
password for the admin user can be found in the configuration manual of the
device.

================================================================================
Serial console
================================================================================

To interrogate the switch to determine which network address it has been
assigned when this information cannot be found by other means, connect a serial
cable to the console port of the switch, log in, and retrieve the network
configuration.

The console port is at the rear of the switch, below the management network
interface:

.. image:: network-console.png
  :width: 660
  :alt: Diagram showing the network and console ports.

The parameters for the serial connection is 115200 baudrate, 8 bits, no parity
and 1 stop bit (**115200 8n1**).

When connected to the console and no text is seen, press ENTER to see whether a
prompt appears.

================================================================================
IP address
================================================================================

To display the IP address of the management port on the switch, issue the
``show interface mgmt`` command.  The output of the command can be piped
through ``grep`` to only display the desired information.

.. code-block:: none

    OS10# show interface mgmt | grep Internet
    Internet address is 10.0.0.1/24
    OS10#

To manually assign an address, in the case where DHCP is not available, or when
the DHCP server assigns an unsuitable address, the following commands can be
used to temporarily assign an address to the management port:

#. Configure the management interface from CONFIGURATION mode. ::

       interface mgmt 1/1/1

#. By default, DHCP client is enabled on the Management interface. Disable the
   DHCP client operations in INTERFACE mode. ::

       no ip address dhcp

#. Configure an IPv4 or IPv6 address on the Management interface in INTERFACE
   mode. ::

       ip address A.B.C.D/mask
       ipv6 address A:B/prefix-length

#. Enable the Management interface in INTERFACE mode. ::

       no shutdown

Configure the management interface with IP address 10.1.1.10, with a netmask of
255.255.255.0:

.. code-block:: none

    OS10# configure terminal
    OS10(config)# interface mgmt 1/1/1
    OS10(conf-if-ma-1/1/1)# no ip address dhcp
    OS10(conf-if-ma-1/1/1)# ip address 10.1.1.10/24
    OS10(conf-if-ma-1/1/1)# no shutdown
    OS10# end

Note that this configuration is not persistent and the configuration will
revert to the previous configuration when the switch is power-cycled.

To make this configuration persistent, issue the following command:

.. code-block:: none

    OS10# copy running-configuration startup-configuration

================================================================================
SSH public key authentication
================================================================================

It is also possible to configure SSH public-key authentication from the switch
CLI.  Issue the command ``system bash`` to launch a bash prompt and add the
relevant SSH public-key to the ``~/.ssh/authorized_keys`` file. ::

    OS10# system bash
    admin@OS10:/home/admin$ echo "ssh-ed25519 AA...XX a.n.other@example.com" >> ~/.ssh/authorized_keys
