********************************************************************************
Upgrade the Kubernetes cluster
********************************************************************************

.. note::
    
    Although the upgrade process is mostly automated through an Ansible playbook, please familiarize yourself with the `kubeadm upgrade procedure`_.
    Basic understanding of what the Ansible playbook is doing is required to be able to resolve any issues the upgrade process may run into.

Upgrading the Kubernetes cluster is a mostly automated process, with an Ansible playbook doing most of the heavy lifting.

Setting the Kubernetes version to upgrade to
============================================

The first step is to configure the target Kubernetes version.

Beware of the following:

- Updates between patch versions, for example ``1.26.4`` to ``1.26.9`` can be done in a single step.
- Updates between minor versions can not jump more than one minor, for example ``1.26.9`` to ``1.27.6`` can be done but ``1.26.4`` to ``1.28.2`` cannot.
  At least two separate updates will be required.

.. note::

    Even if you are updating between consecutive minor versions, it is always best to first make sure the current minor version is updated to the latest patch version first.

After choosing a suitable version to upgrade to, update the ``k8s_version`` Ansible variable to match it and continue to the next step.

Performing the upgrade
======================

To perform the upgrade, run the ``k8s-upgrade.yml`` Ansible playbook using:

.. code-block:: console

    make run playbook=playbooks/k8s-upgrade.yml

This will attempt to upgrade the cluster nodes one-by-one, starting with the control plane nodes.
It is advised to keep an eye on the cluster state while the upgrade process is running, so that you can abort the playbook if something goes wrong.

.. _kubeadm upgrade procedure: https://kubernetes.io/docs/tasks/administer-cluster/kubeadm/kubeadm-upgrade/
