********************************************************************************
Managing the LOW-CBF P4 switches
********************************************************************************

The P4 switches used by LOW-CBF require the installation of a proprietary Software Development Environment (SDE).
This SDE is used to:

* Compile the P4 code from LOW.CBF
* Interact with the P4 switch from the LOW.CBF TANGO devices

.. _p4-init-guide:

Initialising P4 switches
========================

Before initialising P4 switches, make sure they are running Ubuntu 22.04 LTS.

Run the Ansible playbook
------------------------

To perform the installation of the SDE, run the following ``make`` command:

.. code-block:: console

    make run playbook=playbooks/p4-init.yml

This will perform the following on each P4 switch in the Ansible inventory:

* Install required packages
* Copy the SDE tarballs if they do not exist yet
* Extract and compile the SDE on the switch
* Install the compiled kernel modules
* Reboot the switch
* Install the ``switchd`` service
* Checkout and compile the LOW-CBF P4 code
* Start the ``switchd`` service

.. note::

    Compiling the SDE will take a long time at first: between 1 and 3 hours.
    Subsequent compilations are a bit faster, somehwere around 10 minutes.

    To speed up subsequent runs of the Ansible playbook, the SDE is only compiled once, and skipped if the playbook detects an existing installation.

    It is possible to force a recompile of the SDE by passing ``P4_SDE_RECOMPILE=true`` to the ``make`` command.

After the Ansible playbook finishes, you can verify whether everything is installed correctly by connecting to a shell on the P4 switch and checking whether the ``switchd`` service is running:

.. code-block:: console

    sudo systemctl status switchd.service


Updating the LOW-CBF P4 code
============================

To update the LOW-CBF P4 code used on the P4 switches, change the ``low_cbf_p4_code_ref`` variable in the inventory to point to the version you want to update to.
Then, run the Ansible playbook with the following ``make`` command:

.. code-block:: console

    $ make run playbook=playbooks/p4-update-low-cbf.yml

.. note::

    Make sure that the new version of the P4 code is compatible with the current SDE version.
    If this is not the case, change the ``low_cbf_p4_sde_version`` variable in the inventory to point to the correct SDE version.
    Then, update the SDE and the P4 code by following :ref:`p4-init-guide`.

Updating the SDE
================

Before you can update the SDE on the P4 switches, you first have to obtain a copy of it.
A license should already be procured as a part of `SST-790`_.
Assuming you have the necessary account details, download the SDE at `the P4 Studio download page`_.

The download should contain the following tarballs:

* ``bf-sde-<version>.tgz``
* ``bf-reference-bsp-<version>.tgz``

These files should be uploaded to the :ref:`nexus-p4-repo`.

.. warning:: 

    Under no circumstances should the SDE ever be published anywhere public.

After uploading the SDE to Nexus, update the ``low_cbf_p4_sde_version`` Ansible variable to contain the new SDE version, and run:

.. code-block:: console

    make run playbook=playbooks/p4-init.yml

.. _SST-790: https://jira.skatelescope.org/browse/SST-790
.. _the P4 Studio download page: https://www.intel.com/content/www/us/en/secure/confidential/collections/programmable-ethernet-switch-products/p4-suite/p4-studio.html
