.. _deploy-ceph:

********************************************************************************
Deploy the Ceph storage cluster
********************************************************************************

To (re-)deploy the Ceph cluster, run the following ``make`` commands:

.. code-block:: console

    make playbooks oci docker PLAYBOOKS_HOSTS=ceph
    make playbooks ceph install PLAYBOOKS_HOSTS=ceph

.. note::

    The ``make playbooks ceph install`` command is known to fail when creating
    the OSDs on hosts other than the one it uses for bootstrapping the cluster.

    In this case, re-running the command usually succeeds and the Ceph cluster
    should work fine afterwards.

.. note::

    The Ansible collection used to deploy Ceph uses ``cephadm`` under the hood.
    While it's technically possible to operate the Ceph cluster manually from
    one of the nodes using ``cephadm``, any changes made this way will not be
    persisted in case the cluster nodes are reprovisioned and Ceph is 
    redeployed.

    It is therefore not advisable to operate the Ceph cluster in this way.
    Use the Ansible collection to configure the Ceph cluster instead.
