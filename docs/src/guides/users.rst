********************************************************************************
Manage user accounts
********************************************************************************

All user accounts (except the default ``ubuntu`` account) are managed through a custom ``users`` Ansible role.
To synchronize the user accounts defined in the Ansible variables with the hosts in our inventory, run:

.. code-block:: console
    
    make run playbook=playbooks/users.yml

Users are defined in a dictionary called ``users``, where the dictionary key corresponds to the user name,
and the value contains settings for that user.

The following properties can be set for each user:

.. list-table::
    :header-rows: 1

    * - Property
      - Description
    * - ``admin``
      - Setting this to ``true`` will add the user to the sudoers on the system.
        Also, the user's SSH key(s) will be added to ``/home/ubuntu/.ssh/authorized_keys``, enabling the user to use the default ``ubuntu`` user.
    * - ``groups``
      - Groups to add the user to.
        These groups must be defined on the system prior to adding the users.
        Uses a default value if not provided.
    * - ``ssh_key``
      - Used to add a single SSH key to the user's ``.ssh/authorized_keys`` file.
    * - ``ssh_keys``
      - Used to add multiple SSH keys to the user's ``.ssh/authorized_keys`` file.

So, to add a user named ``foo``, define the following:

.. code-block:: yaml

    users:
        foo:
            ssh_key: ssh-rsa abcdef...

Or, a more complex example:

.. code-block:: yaml

    users:
        foo:
            admin: true
            groups: custom,groups,to,assign
            ssh_keys:
                - "{{ lookup('file', 'path/to/first/key.pub') }}"
                - "{{ lookup('file', 'path/to/second/key.pub') }}"
                - ssh-rsa inline-key
