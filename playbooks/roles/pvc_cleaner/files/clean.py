#!/usr/bin/env python

import argparse
import pathlib
import logging
from datetime import datetime, timedelta

logger = logging.getLogger()


def clean(
    root: pathlib.Path,
    retention: int,
    remove_empty_dirs: bool = False,
):
    cutoff = datetime.now() - timedelta(days=retention)
    cutoff_time = cutoff.timestamp()
    logger.info("Cleaning up data in %s not modified since %s", root.as_posix(), cutoff)

    for path, dirs, files in root.walk(top_down=False):
        for file in files:
            file_path = path / file
            if file_path.stat().st_mtime >= cutoff_time:
                logger.debug(
                    "Skipping file %s: last modification time does not meet cutoff",
                    file_path.as_posix(),
                )
                continue

            logger.info("Removing file: %s", file_path.as_posix())
            file_path.unlink()

        if not remove_empty_dirs:
            continue

        for dir in dirs:
            dir_path = path / dir
            if any(dir_path.iterdir()):
                logger.debug("Skipping directory %s: not empty", dir_path.as_posix())
                continue

            logger.info("Removing directory: %s", dir_path.as_posix())
            dir_path.rmdir()


def main():
    parser = argparse.ArgumentParser()

    parser.add_argument(
        "root",
        type=pathlib.Path,
        help="Path to root directory to clean",
    )

    parser.add_argument(
        "--retention",
        type=int,
        required=True,
        help="Number of days to retain data before deleting",
    )

    parser.add_argument(
        "--remove-empty-dirs",
        action="store_true",
        help="When enabled, will remove empty directories",
    )

    parser.add_argument("-v", "--verbose", action="store_true")

    args = parser.parse_args()

    log_level = logging.DEBUG if args.verbose else logging.INFO
    logging.basicConfig(level=log_level)

    root: pathlib.Path = args.root
    retention: int = args.retention

    if not root.exists():
        parser.error(f"Root directory does not exist: {root.as_posix()}")

    if retention < 1:
        parser.error(f"Retention cannot be less than 1 day")

    clean(root, retention, remove_empty_dirs=args.remove_empty_dirs)


if __name__ == "__main__":
    main()
