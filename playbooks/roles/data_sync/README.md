# Ansible role: `data_sync`

This Ansible role sets up a Kubernetes DaemonSet to automatically synchronize a host directory to a Ceph volume using [lsyncd](https://lsyncd.github.io/lsyncd/).

Requirements:

- The Kubernetes cluster should have the `ceph-cephfs` `StorageClass` defined that auto-provisions a `PersistentVolume` backed by `CephFS`.
